-- Databricks notebook source
SELECT * FROM `hive_metastore`.`default`.`height_weight_2k`;

-- COMMAND ----------

/*OPTIMIZE table
Will not work because it isn't a Delta Table
*/
OPTIMIZE `hive_metastore`.`default`.`height_weight_2k`;

-- COMMAND ----------

-- Clone the source table as a Delta Lake table
CREATE TABLE delta_table_height_weight_2k
  USING delta
  AS SELECT *
     FROM `hive_metastore`.`default`.`height_weight_2k`;

-- COMMAND ----------

-- OPTIMIZE table
OPTIMIZE delta_table_height_weight_2k;

-- COMMAND ----------


