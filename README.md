# Databricks-Certified-Data-Engineer

## Code examples


### IntelliJ IDE

Setup and run Go SDK:  https://github.com/databricks/databricks-sdk-go
cd into `gohello`: `go run main.go`

### Spark SQL

```sparksql
SELECT
  *
FROM
  samples.nyctaxi.trips
LIMIT
  10
```

Add Table Documentation

```sparksql
ALTER TABLE
  my_table
SET
  TBLPROPERTIES ('comment' = 'This is a table comment.')
;
```

Query Table Documentation
```sparksql
DESCRIBE EXTENDED my_table
;
```



### CLI

* Install the CLI
```bash
brew tap databricks/tap
brew install databricks
```
* `databricks configure`

* Try out some commands
```bash
➜  databricks-certified-data-engineer git:(main) ✗ databricks clusters list                            
ID                    Name                                               State
1122-183534-k6a4drv0  ML Cluster                                         RUNNING
1101-180550-p6s7bx7c  Thrifty                                            RUNNING
1101-175900-8fdt7eh8  General Cluster                                    TERMINATED
1108-193140-zap2xx03  job-406624195109779-run-4916024586712-Job_cluster  TERMINATED

```

* Query the cluster for information
```bash
databricks clusters get 1122-183534-k6a4drv0 | jq -r .node_type_id
```

* Terminate a cluster 

`databricks clusters delete`

* Start a cluster

`databricks clusters start `

### Pipeline Simulation

```python
# Sample input data
raw_data = [
    {'name': 'John', 'age': 20},
    {'name': 'Sarah', 'age': 34},
    {'name': 'Mike', 'age': 55}
]

# Extract function
def extract(raw):
    data = raw.copy() 
    return data

# Transform function  
def transform(extracted):
    for row in extracted:
        row['age_group'] = get_age_group(row['age'])  
    return extracted

def get_age_group(age):
    if age < 30:
        return 'under 30'
    elif age < 45: 
        return '30-45'
    else:
        return 'over 45'

# Load function
def load(transformed):
    print(transformed)

# Pipeline    
def pipeline(data):
    extracted = extract(data)
    transformed = transform(extracted)
    load(transformed)
    
# Run pipeline
pipeline(raw_data)
```

Jobs

```python
import json

# Sample notebook task definition 
notebook_task = json.dumps({
    "notebook_path": "/Users/example-user/notebook"  
})

# Function to print job details
def show_job(job):
    print(f"Id: {job['id']}")
    print(f"Status: {job['status']}")
    print(f"Task: {job['task']}")
    
# Function to create a sample job    
def create_job():
    job = {
        "id": "1",
        "status": "PENDING",  
        "task": json.loads(notebook_task) 
    }
    
    print("Created job:")
    show_job(job)
    return job

# Example job run loop
job = create_job() 

print("Running job...")
job["status"] = "RUNNING"

print("Completed job:") 
job["status"] = "COMPLETED"
show_job(job)
```



## References

* [Scala Intellij](https://docs.databricks.com/en/dev-tools/databricks-connect/scala/intellij-idea.html)
* [Databricks CLI Install](https://docs.databricks.com/en/dev-tools/cli/index.html)
* [Databricks CLI Tutorial](https://docs.databricks.com/en/dev-tools/cli/tutorial.html)