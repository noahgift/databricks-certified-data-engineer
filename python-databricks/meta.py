class DataCatalog:
    def __init__(self):
        self.metadata = {}

    def add_dataset(self, name, metadata):
        if name not in self.metadata:
            self.metadata[name] = metadata
        else:
            raise ValueError("Dataset already exists")

    def get_dataset(self, name):
        return self.metadata.get(name)


# Test the DataCatalog class
catalog = DataCatalog()

# Add datasets with dynamic attributes
attributes1 = {"size": 500, "type": "image"}
attributes2 = {"size": 1000, "format": "csv", "columns": ["name", "age"]}

catalog.add_dataset("dataset1", attributes1)
catalog.add_dataset("dataset2", attributes2)

# Retrieve datasets and verify metadata
print(catalog.get_dataset("dataset1"))  # {'size': 500, 'type': 'image'}
print(
    catalog.get_dataset("dataset2")
)  # {'size': 1000, 'format': 'csv', 'columns': ['name', 'age']}
