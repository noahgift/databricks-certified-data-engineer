/*
Rust ELO Example for UFC Fighters
 */
struct Fighter {
    name: String,
    rating: u16,
}

impl Fighter {
    fn new(name: String, rating: u16) -> Fighter {
        Fighter { name, rating }
    }

    fn update_rating(&mut self, opponent: &Fighter, result: &str) {
        let k = 50;

        if result == "win" {
            let exp_score =
                1.0 / (1.0 + 10f64.powf((opponent.rating as f64 - self.rating as f64) / 400.0));

            let adjustment = k as f64 * (1.0 - exp_score);

            self.rating += adjustment as u16;
        } else if result == "loss" {
            let exp_score =
                1.0 / (1.0 + 10f64.powf((self.rating as f64 - opponent.rating as f64) / 400.0));

            let adjustment = k as f64 * (0.0 - exp_score);

            self.rating += adjustment.round() as u16;
        }
    }
}

fn main() {
    let mut fighter1 = Fighter::new(String::from("Conor McGregor"), 1500);
    let mut fighter2 = Fighter::new(String::from("Khabib Nurmagomedov"), 1500);

    fighter1.update_rating(&mut fighter2, "loss");
    fighter2.update_rating(&mut fighter1, "win");

    println!("Name: {}, Rating:{}", fighter1.name, fighter1.rating);
    println!("Name: {}, Rating:{}", fighter2.name, fighter2.rating);
}
